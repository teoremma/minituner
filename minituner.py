import numpy as np
import pyaudio

# Parameter definitions

RATE = 22050
CHANNELS = 1
FRAME_SIZE = 2048
FRAMES_PER_BUFFER = 16
BUFFER_SIZE = FRAME_SIZE * FRAMES_PER_BUFFER
FREQ_STEP = float(RATE) / BUFFER_SIZE
MIN_FREQ = 20
FIRST_IND = round(MIN_FREQ / FREQ_STEP)
A4_FREQ = 440.0
TIME_PER_FRAME = FRAME_SIZE / RATE

NOTES = "A A# B C C# D D# E F F# G G#".split()

def closest_note(f): return 48 + int(np.round(12 * np.log2(f / A4_FREQ)))
def note_freq(n): return A4_FREQ * 2.0 ** ((n - 48) / 12.0)
def note_name(n): return NOTES[n % 12] + str(n // 12)

stream = pyaudio.PyAudio().open(
    format=pyaudio.paInt16,
    channels=CHANNELS,
    rate=RATE,
    input=True,
    frames_per_buffer=FRAME_SIZE)

# Hanning filter 
hann_win = 0.5 * (1 - np.cos(np.linspace(0, 2 * np.pi, BUFFER_SIZE, False)))
buf = np.zeros(BUFFER_SIZE)

stream.start_stream()
while stream.is_active():
    frame = np.frombuffer(stream.read(FRAME_SIZE), np.int16)
    buf[:-FRAME_SIZE] = buf[FRAME_SIZE:]
    buf[-FRAME_SIZE:] = frame
    
    fft = np.abs(np.fft.rfft(buf * hann_win))

    # Get the max index ignoring low frequencies
    max_ind = fft[FIRST_IND:].argmax() + FIRST_IND

    # If we can apply quadratic interpolation
    if max_ind > 0 and max_ind < len(fft) - 1:
        a = fft[max_ind - 1]
        b = fft[max_ind]
        c = fft[max_ind + 1]
        d = (c - a) / (2 * (2 * b - a - c))
        fund = (max_ind + d) * FREQ_STEP
    else:
        fund = max_ind * FREQ_STEP
        
    closest = closest_note(fund)
    closest_freq = note_freq(closest)

    print("{:.2f}\t{:>3s} {:+.2f}".format(
        fund, note_name(closest), fund - closest_freq))

